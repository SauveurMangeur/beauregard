from dash import html
from model import MeteoJour
from meteoblue import MeteoBlue

meteoblue = MeteoBlue()

def affiche_meteo_jour(meteo : MeteoJour) -> html.Div:
    """
    Affiche les prévisions météo d'un jour
    """
    return html.Div([
        html.H2(f"{meteo.date.date()}"),
        html.Div([
            html.P(f"Température : {meteo.temperature}°C", className="flexChild"),
            html.P(f"Précipitations : {meteo.precipitation}mm", className="flexChild")
        ], className="flex")
    ], className="flexChild")


# Affichage de la météo
meteo_jour = html.Div([
            html.H2('Météo'),
            html.Div([affiche_meteo_jour(meteo) for meteo in meteoblue.meteo_previsions()], id="meteo-jours")


        ], id="meteo")