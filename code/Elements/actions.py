from dash import Dash, dcc, html, Input, Output
import dash_daq as daq

# Interrupteurs pour les radiateurs, le ballon et le groupe électrogène
actions = html.Div([
    html.H2('Actions'),
    daq.ToggleSwitch(id='radiateur', label='Radiateur'),
    daq.ToggleSwitch(id='ballon', label='Ballon'),
    daq.ToggleSwitch(id='groupe', label='Groupe électrogène')
], id="actions")
