from dash import Dash, dcc, html, Input, Output


consommation_électrique = html.Div([
    html.H2('Consommation d\'énergie'),
    html.Div([
        html.P("Maison :", className="flexChild"),
        html.P("Ballon : ", className="flexChild"),
        html.P("Sol SdB :", className="flexChild"),
        html.P("Radiateurs : ", className="flexChild")
    ], className="flex")
], id="consommation-électrique")