import requests
import requests_cache
from datetime import datetime, tzinfo, timedelta
from zoneinfo import ZoneInfo
from meteoblueresult import Result

from model import MeteoJour


class MeteoBlue:
    """
    Sert à récupérer les données météo d'un lieu donné en utilisant les coordonnées GPS

    Par défaut utilise les coordonnées de Beauregard.
    Une fois initialisé, plusieurs appels peuvent être faits, météo générale et données solaire
    """

    # Met en place un cache d'une heure, pour économiser des crédits
    requests_cache.install_cache('meteoblue_cache', backend='sqlite', expire_after=3600)

    def __init__(self, api_key="WMFYiohSnup148Pf", longitude=6.61688248410553, latitude=46.16914155302416):
        self.api_key = api_key
        self.longitude = longitude
        self.latitude = latitude
        self.base_url = "https://my.meteoblue.com/packages/basic-1h_basic-day_solar-1h"

    def __weather_data(self):
        """
        Récupère les données météo en utilisant l'API météoblue.
        """

        # Construct the API URL
        url = f"{self.base_url}?lat={self.latitude}&lon={self.longitude}&apikey={self.api_key}&format=json"
        response = requests.get(url)
        # Vérifie que la requête a bien le format attendu
        return Result(**response.json())

    def sunshine_previsions(self):
        """
        Récupère les prévisions de soleil
        """

        # Récupère les données météo
        weather_data: Result = self.__weather_data()

        # Récupère le fuseau horaire
        timezone: tzinfo = ZoneInfo(weather_data.metadata.timezone_abbrevation)

        # Récupère les données météorologiques
        solar_data = weather_data.data_1h

        # Construit la liste des heures et des prévisions de soleil
        hours = []
        sunshine = []
        for i, str_time in enumerate(solar_data.time):
            # Convertit la date en heure locale
            date = datetime.fromisoformat(str_time).astimezone(timezone)
            # Ajoute l'heure locale à la liste des heures
            hours.append(date)
            # Ajoute la prévision de soleil à la liste
            sunshine.append(solar_data.gni_instant[i])

        return hours, sunshine

    def energie_futur(self, nombre_heures : int) -> float:
        """
        Calcule l'énergie reçu par les panneaux solaires dans les nombre_heures qui suivent l'heure actuel.
        :param nombre_heures: Le nombre d'heures à prendre en compte
        :return: L'énergie reçue par les panneaux solaires en Wh.m-2
        """
        now = datetime.now()
        # Récupère les prévisions de soleil
        hours, sunshine = self.sunshine_previsions()
        # Calcule l'énergie reçu par les panneaux solaires
        energie = 0
        for i, hour in enumerate(hours):
            if now <= hour < now + timedelta(hours=nombre_heures):
                energie += sunshine[i]
        return energie

    def meteo_previsions(self) -> list[MeteoJour]:
        """
        Récupère les prévisions de météo des journées qui suivent
        """

        # Récupère les données météo journalières
        weather_data_daily = self.__weather_data().data_day


        # Construit la liste des prévisions de météo
        meteo_previsions = []
        for i, str_time in enumerate(weather_data_daily.time):
            date = datetime.fromisoformat(str_time)
            # Créé la prévision de météo
            meteo_jour = MeteoJour(date=date,
                                   temperature=weather_data_daily.temperature_mean[i],
                                   precipitation=weather_data_daily.precipitation[i],
                                   wind_speed=weather_data_daily.windspeed_mean[i],
                                   wind_direction=weather_data_daily.winddirection[i],
                                   relative_humidity=weather_data_daily.relativehumidity_mean[i],
                                   pressure=weather_data_daily.sealevelpressure_mean[i],
                                   precipitation_probability=weather_data_daily.precipitation_probability[i])

            # Ajoute la prévision de météo à la liste
            meteo_previsions.append(meteo_jour)
        return meteo_previsions
