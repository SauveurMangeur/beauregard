import datetime

from dash import Dash, dcc, html, Input, Output
import plotly.graph_objects as go
from meteoblue import MeteoBlue
from Elements.consommation_électrique import consommation_électrique
from Elements.meteo_jours import meteo_jour
from relais import Relai
import dash_daq as daq

relai = Relai()

actions = html.Div([
    html.H2('Actions'),
    daq.PowerButton(id='ballon', label='Ballon', on=relai.ballon.is_active),
    daq.PowerButton(id='raiponce', label='Salle de bain Raiponce', on=relai.salle_de_bain_raiponce.is_active),
    daq.PowerButton(id='epilobe', label='Salle de bain Epilobe', on=relai.salle_de_bain_epilobe.is_active),
    daq.PowerButton(id='ancolie', label='Salle de bain Ancolie', on=relai.salle_de_bain_ancolie.is_active),
    daq.PowerButton(id='jocbarbe', label='Salle de bain Jocbarbe', on=relai.salle_de_bain_jocbarbe.is_active),
    daq.PowerButton(id='serpolet', label='Salle de bain Serpolet', on=relai.salle_de_bain_Serpolet.is_active)
], id="actions")

app = Dash(__name__)
meteo = MeteoBlue()

app.layout = html.Div([
    html.H1('Bienvenue à Beauregard'),
    html.Div([
        # Affichage de l'heure
        html.Div([
            html.H2('Heure locale'),
            html.Div(id="local-time"),
            dcc.Interval(
                id='interval-component',
                interval=1 * 1000,  # in milliseconds
                n_intervals=0
            )

        ], id="heure"),
        meteo_jour,
        actions,
        # Graphique de la production solaire
        html.Div([
            html.H2('Production solaire'),
            dcc.Graph(id="graph-solaire"),
            html.P("Number of bars:")
        ], id="production-solaire"),

        # Graphique de la consommation d'énergie
        consommation_électrique,
    ], id="main-grid"),
    dcc.Slider(id="slider", min=2, max=10, value=4, step=1),
])


@app.callback(
    Output("graph-solaire", "figure"),
    Input("slider", "value"))
def update_bar_chart(size):
    time, sunshine = meteo.sunshine_previsions()
    time = time[:size*24]
    sunshine = sunshine[:size*24]
    print(sunshine)
    fig = go.Figure(
        data=[go.Scatter(x=time, y=sunshine)],
        layout_title_text="Radiation solaire horaire"
    )
    return fig


@app.callback(Output('local-time', 'children'),
              Input('interval-component', 'n_intervals'))
def update_metrics(n):
    return html.H2(datetime.datetime.now().strftime("%H:%M:%S"))


@app.callback(
    Output("ballon", "label"),
    Input("ballon", "on"))
def toggle_ballon(on: bool):
    print(relai.ballon.value)
    if on:
        relai.ballon.off()
        return "Ballon allumé"
    relai.ballon.on()
    return "ballon éteint"

@app.callback(
    Output("raiponce", "label"),
    Input("raiponce", "on"))
def toggle_raiponce(on: bool):
    if on:
        relai.salle_de_bain_raiponce.off()
        return "Salle de bain Raiponce allumé"
    relai.salle_de_bain_raiponce.on()
    return "Salle de bain Raiponce éteint"

@app.callback(
    Output("epilobe", "label"),
    Input("epilobe", "on"))
def toggle_epilobe(on: bool):
    if on:
        relai.salle_de_bain_epilobe.off()
        return "Salle de bain Epilobe allumé"
    relai.salle_de_bain_epilobe.on()
    return "Salle de bain Epilobe éteint"

@app.callback(
    Output("ancolie", "label"),
    Input("ancolie", "on"))
def toggle_ancolie(on: bool):
    if on:
        relai.salle_de_bain_ancolie.off()
        return "Salle de bain Ancolie allumé"
    relai.salle_de_bain_ancolie.on()
    return "Salle de bain Ancolie éteint"

@app.callback(
    Output("jocbarbe", "label"),
    Input("jocbarbe", "on"))
def toggle_jocbarbe(on: bool):
    if on:
        relai.salle_de_bain_jocbarbe.off()
        return "Salle de bain Jocbarbe allumé"
    relai.salle_de_bain_jocbarbe.on()
    return "Salle de bain Jocbarbe éteint"

@app.callback(
    Output("serpolet", "label"),
    Input("serpolet", "on"))
def toggle_serpolet(on: bool):
    if on:
        relai.salle_de_bain_Serpolet.off()
        return "Salle de bain Serpolet allumé"
    relai.salle_de_bain_Serpolet.on()
    return "Salle de bain Serpolet éteint"



app.run(debug=True, host="0.0.0.0")
