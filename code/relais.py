from gpiozero import LED

class Relai:
    def __init__(self):
        Relai_1 = LED(5)
        Relai_2 = LED(6)
        Relai_3 = LED(13)
        Relai_4 = LED(19)
        Relai_5 = LED(26)
        Relai_6 = LED(12)

        self.ballon = Relai_6
        self.salle_de_bain_raiponce = Relai_5
        self.salle_de_bain_epilobe = Relai_4
        self.salle_de_bain_ancolie = Relai_3
        self.salle_de_bain_jocbarbe = Relai_2
        self.salle_de_bain_Serpolet = Relai_1


