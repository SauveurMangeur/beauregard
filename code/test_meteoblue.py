import datetime
from unittest import TestCase
from zoneinfo import ZoneInfo
from meteoblue import MeteoBlue


class TestMeteoBlue(TestCase):
    # initialistation de la classe meteoblue
    def setUp(self) -> None:
        self.meteoblue = MeteoBlue()

    def test_suntime_data_is_from_today(self):
        times, sunshines = self.meteoblue.sunshine_previsions()

        self.assertEqual(times[0], datetime.datetime
                         .now(tz=ZoneInfo("Europe/Zurich"))
                         .replace(microsecond=0, second=0, minute=0, hour=0))


